import React, { Component } from 'react';

class FlyoutMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isHovered: false,
        };
        this.handleHover = this.handleHover.bind(this);
    }

    handleHover() {
        const  currentState = this.state.isHovered;
        this.setState({isHovered: !currentState});
    }
    render() {
        const mainLink = this.props.name;
        const link = this.props.link;
        const iterKey = this.props.iterkey;

        const innerMenu = this.props.menu? this.props.menu : '';

        const isEmpty = innerMenu === 'undefined' || innerMenu === null || innerMenu === ''? true : false;

        if(isEmpty) {
            return <div></div>
        }

        const smallMenu = innerMenu.map((subItem, key) => {
            return (
                <div key={key}>
                    <a href={subItem.link}>
                        <div  className="flyout-subItem" >{subItem.name}</div>
                    </a>
                </div>)
        });

        return(
            <div className='flyout-main' key={iterKey} onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}>
                <div className='MainMenuItem'><a href={link}>{mainLink}</a></div>
                {/*<i className={this.state.isActive?'has-content':'hide-content'}></i>*/}
                <div className={this.state.isHovered? 'flyout-menu show' : 'hidden'} >
                    {smallMenu}
                </div>
            </div>
        );
    }
}
export default FlyoutMenu;