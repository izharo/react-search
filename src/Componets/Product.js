import React, { Component } from 'react';

import '../css/product.css';
class Product extends Component {

    render() {
        const searchKey = this.props.searchFilter ? this.props.searchFilter : '';
        const productArray = this.props.products;

        let productList = productArray.map(
            (item) => {

                if(searchKey === item.id){
                    return   <ListItem key={item.id} id={item.id} value={item.productItem} />
                }

                if(this.props.searchFilter.length === 0){
                    return <ListItem key={item.id} id={item.id} value={item.productItem} />

                } else {
                  return null
                }
            }
        );

        return (
            <div className='container'>
                <div className='product-list'>
                    {productList}
                </div>
            </div>
        );
    }
}

function ListItem(props) {
    return (
        <div className='list-item'>
            <div className='list-id'>id: {props.id}</div>
            <div className='list-prod'> Item: {props.value}</div>
        </div>
    );
}
export default Product;
