import React, { Component } from 'react';

import '../css/input.css';

class Header extends Component {

    constructor (props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
    }

    handleChange (event) {
        this.props.onUpdateSearch(event.target.value);
    }
    handleToggle (event) {
        this.props.onToggle(event.target.value);
    }

    render() {
        return (
            <div id='header'>
                <div id='btn-toggle'>Flyout Menu
                    <label className="switch">
                        <input type="checkbox"/>
                            <span className="slider round" onClick={this.handleToggle}></span>
                    </label>
                </div>
            <form className='search-input'>
                <label>
                    <input
                        id='itemSearch'
                        type="text"
                        value={this.props.searchFilter}
                        onChange={ this.handleChange}
                    />Search item id &nbsp;
                </label>
            </form>
            </div>
        );
    }
}

export default Header;
