import React, { Component } from 'react';

import jsonMenu from '../json/menu';
import AccordMenu from './AccordMenu';

import '../css/leftNav.css';
import FlyoutMenu from "./FlyoutMenu";

class LeftMenu extends Component {

    constructor (props) {
        super(props);
        this.state = {
            jsonMenu,
            search : "",
            activeTab:0
        };
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    onClickHandler() {
        this.setState({
            activeTab: this.state.activeTab===1? 0 : 1
        });
    }

    onChange = e => {
        this.setState({
            search : e.target.value
        });
    };

    render(){

        const flyout = this.props.flyoutMenu;

        console.log('leftMenu props- ' + this.props.flyoutMenu);

        let subList;
        const {search} = this.state;
        const searchList = jsonMenu.menu.filter( item => {

            console.log('item - ' + item);
            console.log('sub item -- ' + item.sub);
            console.log('sub condition --- ' + item.sub !== null );

            if(item.sub != null ){
                console.log('inside sub');
                const subArray = item.sub;
                
                subList = subArray.filter(subitem =>{
                    return subitem.name.toLowerCase().indexOf(search.toLowerCase()) !== -1
                });
            }
            console.log(subList);
            if(subList === search){
                return subList;
            }else {
                return item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1
            }
            });

        console.log('search - ' + searchList);

        return (
            <div id="leftNav">
                <div id='menu-search'>
                    <label>
                        <i className="fa fa-search"></i>
                        <input onChange={this.onChange} />
                    </label>
                </div>
                { searchList.map((item, i) => (
                    (item.sub === null) ? (
                    <div className="MainMenuItem" key={i}><a href={item.link}>{item.name}</a></div>
                        ):(
                        (flyout) ? (
                            <FlyoutMenu
                                className="flyout-menu"
                                key={i}
                                iterkey={i}
                                menu={item.sub}
                                link={item.link}
                                name={item.name}
                            />
                            ): (
                            <AccordMenu
                                className="MainMenuItem"
                                key={i}
                                iterkey={i}
                                menu={item.sub}
                                link={item.link}
                                name={item.name}/>
                        )
            )))}
            </div>
        );
    }
}

export default LeftMenu;
