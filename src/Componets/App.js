import React, { Component } from 'react';


import Product from './Product';
import Header from './Header';
import LeftMenu from './LeftMenu';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products : [
                {id:'1', productItem:"a"},
                {id:'2', productItem:"b"},
                {id:'3', productItem:"c"},
                {id:'4', productItem:"d"},
                {id:'5', productItem:"e"},
                {id:'6', productItem:"f"},
                {id:'7', productItem:"g"},
                {id:'8', productItem:"h"},
                {id:'9', productItem:"i"},
                {id:'10', productItem:"j"},
                {id:'11', productItem:"k"},
                {id:'12', productItem:"l"},
                {id:'13', productItem:"m"},
                {id:'14', productItem:"n"},
                {id:'15', productItem:"o"},
                {id:'16', productItem:"p"},
                {id:'17', productItem:"q"}
            ],
            searchFilter: '',
            flyout: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onToggleFlyout = this.onToggleFlyout.bind(this);
        // this.state.searchFilter = this.state.products;
    }

  handleInputChange(input){
      this.setState({
          searchFilter: input
      });
  }

    onToggleFlyout() {
        this.setState({
            flyout: this.state.flyout===true? false : true
        });
    }

    render() {
        console.log('app- ' + this.state.flyout );
        return (
            <div>
                <Header
                    onUpdateSearch={this.handleInputChange}
                    searchText={this.state.searchFilter}
                    onToggle={this.onToggleFlyout}
                    flyout={this.state.flyout}/>
                <div id="content">
                    <LeftMenu flyoutMenu={this.state.flyout}/>
                    <Product
                        products={this.state.products}
                        searchFilter={this.state.searchFilter} />
                </div>
            </div>
        );
    }
}


export default App;
