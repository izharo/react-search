import React, { Component } from 'react';
// import jsonMenu from '../json/menu'

class AccordMenu extends Component {

    constructor(props){
        super(props);
        this.state= {
            isActive: false,
        };
        this.toggleShow = this.toggleShow.bind(this);
    }

    toggleShow() {
        const currentState = this.state.isActive;
        this.setState({ isActive: !currentState });
    };

    render(){

        const mainLink = this.props.name;
        // const link = this.props.link;
        const iterKey = this.props.iterkey;

        const innerMenu = this.props.menu? this.props.menu : '';

        // console.log(this.state.flyout);

        const isEmpty = innerMenu === 'undefined' || innerMenu === null || innerMenu === ''? true : false;

        // console.log(isEmpty);

        if(isEmpty) {
            return <div></div>
        }

        const smallMenu = innerMenu.map((subItem, key) => {
            return <div key={key} className={this.state.isActive?'show':'hidden'}><a href={subItem.link}><div  className="subItem" >{subItem.name}</div></a></div>
        });

        return(
            <div key={iterKey}>
                <div className={'MainMenuItem'} onClick={this.toggleShow}>{mainLink}</div><i className={this.state.isActive?'has-content':'hide-content'}></i>
                <div >
                    {smallMenu}
                </div>
            </div>
        );
    }
}

// function littleMenu (props) {
//     return <div>{props}</div>
// }
export default AccordMenu;
